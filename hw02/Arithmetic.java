public class Arithmetic{
  
  public static void main(String args[]){
    
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pantsdouble pantsPrice = 34.98;
    double pantsPrice = 34.98;
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    
    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;
    
    //the tax rate
    double paSalesTax = 0.06;
    //total cost of pants 
    double totalCostPants;
    //total cost of shirts
    double totalCostShirts;
    //total cost of belts 
    double totalCostBelts;
    //total sales tax on pants
    double salesTaxOnPants;
    //total sales tax on shirts
    double salesTaxOnShirts;
    //total sales tax on belts
    double salesTaxOnBelts;
    //total cost of entire purchase before tax 
    double totalCostBeforeTax;
    //total sales tax on entire purchase
    double totalSalesTax;
    //total cost of entire purchase after tax
    double totalCostAfterTax;
      
    totalCostPants = numPants * pantsPrice;
    totalCostShirts = numShirts * shirtPrice;
    totalCostBelts = numBelts * beltCost;
    
    salesTaxOnPants = (int) (paSalesTax * totalCostPants * 100) / 100.0;
    salesTaxOnShirts = (int) (paSalesTax * totalCostShirts * 100) / 100.0;
    salesTaxOnBelts = (int) (paSalesTax * totalCostBelts * 100) / 100.0;
    
    totalCostBeforeTax = totalCostPants + totalCostShirts + totalCostBelts;
    
    totalSalesTax = (int) ((paSalesTax * totalCostBeforeTax) * 100) / 100.0;
    
    totalCostAfterTax = (int) ((totalCostBeforeTax + totalSalesTax) * 100) / 100.0;
    
    System.out.println("Total cost of pants: " + "$" + totalCostPants);
    System.out.println("Total cost of shirts: " + "$" + totalCostShirts);
    System.out.println("Total cost of belts: " +  "$" + totalCostBelts);
    System.out.println("Sales tax on pants: " +  "$" + salesTaxOnPants);
    System.out.println("Sales tax on shirts: " +  "$" + salesTaxOnShirts);
    System.out.println("Sales tax on belts: " +  "$" + salesTaxOnBelts);
    System.out.println("Total cost before tax: " +  "$" + totalCostBeforeTax);
    System.out.println("Total sales tax of purchase: " +  "$" + totalSalesTax);
    System.out.println("Total cost after tax: " +  "$" + totalCostAfterTax);
    
    
    
    
    
    
    
    
  }
    
}