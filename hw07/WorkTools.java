/* Sebastian Soman
 * CSE 002 
 * Work Tools class
 * 
*/

import java.util.Scanner;

public class WordTools{
   private static Scanner scanner = new Scanner(System.in);
   
   //replaces double or more spaces with single spaces
   private static String shortenSpace(String text) {
       String temp = text.trim().replaceAll(" + ", " ");
       return temp;

   }

   //replaces all exclamation marks with character '.' (Period)
 
   private static String replaceExclamation(String text) {
       String temp = text.replaceAll("!", ".");
       return temp;
   }

 
 //finds the number of occurenences of somthing in text and outputs number
  
   private static int findText(String text, String find) {
       int count = 0;
       int i = 0;
      
       while ((i = text.indexOf(find)) != -1) {
           text = text.substring(i + find.length());
           count += 1;
       }

       return count;
   }

   
   //takes a string and returns the number of words in the text.
   
   private static int getNumOfWords(String text) {
       // Split method that returns the array of words
       text = shortenSpace(text);
       String[] words = text.split(" ");

       // return count of words
       return words.length;
   }

 
   //gets NumOfNonWSCharacters that removes the whitepsaces and returns the length of the text
 
   private static int getNumOfNonWSCharacters(String text) {
       text = text.trim().replaceAll("\\s", "");
       return text.length();
   }

  
   //method printMenu that prints a menu of choices and prompts user to enter choice.
   
   private static void printMenu() {
       System.out.println("Menu:");
       System.out.println("c - Number of non-whitespace characters");
       System.out.println("w - Number of words");
       System.out.println("f - Find text");
       System.out.println("r - Replace all !'s");
       System.out.println("s - Shorten spaces");
       System.out.println("q - Quit");
       System.out.println("Choose an option: ");
   }

   public static void main(String[] args) {

       while(true) {
           System.out.println("Enter a sample text: ");
           String text = scanner.nextLine();
           System.out.println("You entered: " + text);
  
           //Display menu
           printMenu();
           char ch = scanner.nextLine().charAt(0);
  
           switch (ch) {
           case 'q':
               System.exit(0);
              
           case 'c':   // Number of non-whitespace characters
               int cntNonWhitespaces = getNumOfNonWSCharacters(text);
               System.out.println("Number of non-whitespace characters: " + cntNonWhitespaces);
               break;
              
           case 'w':   // Number of words
               int wordsCount = getNumOfWords(text);
               System.out.println("Number of words: " + wordsCount);
               break;
              
           case 'f':   // Find text
               System.out.println("Enter a word or phrase to be found: ");
               String find = scanner.nextLine();
               int findCount = findText(text, find);
               System.out.println("\"" + find + "\" instances: " + findCount);
               break;
              
           case 'r':   // Replace all !'s
               String newstring = replaceExclamation(text);
               System.out.println("Edited text: " + newstring);
               break;
              
           case 's':   // Shorten spaces
               newstring = shortenSpace(text);
               System.out.println("Edited text:" + newstring);
               break;
              
           default: //User invalid entry output
               System.out.println("Invalid option. Please try again");
           }
          
           System.out.println();
       }
   }
}
