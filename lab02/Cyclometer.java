/* CSE 02-210 Cyclometer 
Sebastian Soman 
9.5.18
*/

//This program calculates the duration and number of miles of trip 1 and 2 as well as the total distance. 
public class Cyclometer{
  
  public static void main(String[] args){
    int secsTrip1 = 480; //length of trip 1 in seconds
    int secsTrip2 = 3220; //length of trip 2 in seconds
    int countsTrip1 = 1561; //number of rotations for trip 1 
    int countsTrip2 = 9037; //number of rotations for trip 2
    double wheelDiameter = 27.0; // diameter of wheel in inches 
    final double PI = 3.14159; //the value of PI
    double feetPerMile = 5280; //number of feet in a mile
    double inchesPerFoot = 12; //number of inches in a foot
    double secondsPerMinute = 60; //number of seconds in a minute
    double distanceTrip1; //total distance of trip 1
    double distanceTrip2; //total distance of trip 2
    double totalDistance; //total overall distance
    
    System.out.println("Trip 1 took " + (secsTrip1 / secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
    System.out.println("Trip 2 took " + (secsTrip2 / secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
    
    //run the calculations; store the values
    //this calculates the duration of trip 1 and 2
    
    distanceTrip1 = countsTrip1 * wheelDiameter * PI;
    // Above gives distance in inchesPerFoot
    //(for each count, a rotation of the wheel travels
    //the diameter in inches times PI)
    distanceTrip1 /= inchesPerFoot * feetPerMile; //Gives distance in miles
    distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;
    totalDistance = distanceTrip1 + distanceTrip2;
    
    //Print out the output data.Print
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
    
    
   
    
  }
}