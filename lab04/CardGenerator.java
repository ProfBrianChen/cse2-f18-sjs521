/* Sebastian Soman 
   CSE 02 
   CardGenerator Class
   This class picks a random card from a deck of 52 cards and prionts the card identity and suit.This
*/

public class CardGenerator{
  
  public static void main(String args[]){
    
    int randomCard = (int) (Math.random() * 53); //picks a random card
    int identityValue = randomCard % 13; //assigns the random card to its designated card identity
    String cardIdentity = "";
    String suit = "";
    
    //determines the correct suit
    if ((randomCard >= 1) && (randomCard <= 13)){
      suit = "Diamonds";
    }
    else if ((randomCard >= 14) && (randomCard <= 26)){
      suit = "Clubs";
    }
    else if ((randomCard >= 27) && (randomCard <= 39)){
      suit = "Hearts";
    }
    else if ((randomCard >= 40) && (randomCard <= 52)){
      suit = "Spades";
    }
    
    //determines the correct card identity 
    switch (identityValue){
      case 1:
        cardIdentity = "Ace";
        break;
      case 2:
        cardIdentity = "2";
        break;
      case 3: 
        cardIdentity = "3";
        break;
      case 4: 
        cardIdentity = "4";
        break;
      case 5: 
        cardIdentity = "5";
        break;
      case 6: 
        cardIdentity = "6";
        break;
      case 7: 
        cardIdentity = "7";
        break;
      case 8:
        cardIdentity = "8";
        break;
      case 9: 
        cardIdentity = "9";
        break;
      case 10: 
        cardIdentity = "10";
        break;
      case 11:
        cardIdentity = "Jack";
        break;
      case 12: 
        cardIdentity = "Queen";
        break;
      case 13:
        cardIdentity = "King";
        break;
      default:
        cardIdentity = "0";
        break;
    }
    
      System.out.print("You picked the " + cardIdentity + " of " + suit + "."); //prints which card was randomly generated
  
    
    
  }
}