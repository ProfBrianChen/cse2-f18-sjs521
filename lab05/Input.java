/* course number (int)
-department name
-number of times class meets a week
-times of day the course begins in military time
-instructor name
-number of students in class
*/

/* Sebastian Soman
 * CSE 002
 * 10.9.18
 * Input Class
 * This class asks the user for the course number, department name, number of classes per week, starting time, professor name, and number of students in the class. It prints this information at the end.
*/

import java.util.Scanner;

public class Input{
  public static void main(String args[]){
    Scanner scnr = new Scanner(System.in);
    int courseNum = 0;
    String departmentName = "Biology";
    int numOfClasses = 0;
    String time = "12:00";
    String professorName = "Kalafut";
    int numOfStudents = 0;
    String junk = ""; //to clear the buffer
    
    System.out.println("Enter the course number: ");
    while (!scnr.hasNextInt()){ //checks see if course number is an integer
      System.out.println("Please enter an integer: ");
      junk = scnr.nextLine();
    }
    if (scnr.hasNextInt()){
    courseNum = scnr.nextInt(); //assigns courseNum to accetable integer 
    junk = scnr.nextLine();
    }
       
    System.out.println("Enter the department name: ");
    departmentName = scnr.nextLine(); //assigns departmentName to acceptable String
    
    
    System.out.println("Enter the number of times the class meets per week: ");
    while (!scnr.hasNextInt()){ //checks to see if number of times per week is an integer
      System.out.println("Please enter an integer: ");
      junk = scnr.nextLine();
    }
    if (scnr.hasNextInt()){
    numOfClasses = scnr.nextInt();
    junk = scnr.nextLine(); //assigns numOfClasses with acceptable integer
    }
    System.out.println("Enter the time of the class: ");
    time = scnr.nextLine(); //assigns time with acceptable String
    
    System.out.println("Enter the name of the professor: ");
    professorName = scnr.nextLine(); //assigns professorName with acceptable String
    
    System.out.println("Enter the number of students in the course: ");
    while (!scnr.hasNextInt()){ //checks to see if number of students in course is an integer
      System.out.println("Please enter an integer: ");
      junk = scnr.nextLine();
    }
    if (scnr.hasNextInt()){
      numOfStudents = scnr.nextInt(); //assigns numOfStudents with acceptable integer
      junk = scnr.nextLine();
    }
    
    //prints out each course detail on a separate line
    System.out.println("Course number: " + courseNum);
    System.out.println("Department name: " + departmentName);
    System.out.println("Number of times the class meets per week: " + numOfClasses);
    System.out.println("Time of class: " + time);
    System.out.println("Name of professor: " + professorName);
    System.out.println("Number of students in class: " + numOfStudents);
        
  }
}