/*Sebastian Soman
 * CSE 002
 * PatternA class
 * The same notes within the code apply to the 3 other Pattern classes as well
 * */

import java.util.Scanner;

public class PatternA{
  public static void main(String args[]){
    
    Scanner scnr = new Scanner(System.in);
    int input = 0;
    int i = 0;
    int j = 0;
    String junk = "";
    
    //makes sure the user enters an integer
    System.out.println("Enter an integer between 1 and 10: ");
    while (!scnr.hasNextInt()){
      System.out.println("Enter a valid integer: "); //corrects user if they don't enter an integer
      junk = scnr.nextLine();
    }
    //makes sure the integer is between 1 and 10
    if (scnr.hasNextInt()){
      input  = scnr.nextInt();
      junk = scnr.nextLine();
      if (input >= 1 && input <= 10){   
      }
      else{
        System.out.println("Your integer must be between 1 and 10: ");
        input = scnr.nextInt();
      }
    }
    //makes and prints the pattern
    for (i=1; i<=input; i++){
      for (j=1; j<=i; j++){
        System.out.print(j);
        System.out.print(" ");
      }
      System.out.println("");
    }
  }
}
    
      
    
    
    
    
    
  

        
        
        
      
      