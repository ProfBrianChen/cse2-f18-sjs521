/* CSE 02 WelcomeCLass 
Sebastian Soman 9.4.18
This class prints "Welcome" followed by my Lehigh Network ID in a graphic design. Additionally, it prints a short biographical statement about myself.This
*/

public class WelcomeClass{

  public static void main(String args[]){
    
    System.out.println("-----------");
    System.out.println("| WELCOME |");
    System.out.println("-----------");
    System.out.println("  ^  ^  ^  ^  ^  ^  ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-S--J--S--5--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
    System.out.println("  v  v  v  v  v  v");
    System.out.println("My name is Sebastian. I'm from Chicago and I love to spend time with family and friends and my favorite holiday is Christmas");
    
    }
    
}
    