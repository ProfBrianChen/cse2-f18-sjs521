/*Sebastian Soman
 * CSE 02
 * 10.23.18
 * EncryptedX class
 * This class takes user input for a length of the encrypted X design then prints it.
 * */

import java.util.Scanner;

public class EncryptedX{
  public static void main(String args[]){
    
    Scanner scnr = new Scanner(System.in);
    int input = 0;
    int i = 0;
    int j = 0;
    String junk = "";
    
    //makes sure the user enters an integer
    System.out.println("Enter an integer between 0 and 100: ");
    while (!scnr.hasNextInt()){
      System.out.println("Enter a valid integer: "); //corrects user if they don't enter an integer
      junk = scnr.nextLine();
    }
    //makes sure the integer is between 0 and 100
    if (scnr.hasNextInt()){
      input  = scnr.nextInt();
      junk = scnr.nextLine();
      if (input >= 0 && input <= 100){   
      }
      else{
        System.out.println("Your integer must be between 1 and 10: ");
        input = scnr.nextInt();
      }
    }
    //designs and prints the pattern
    for (i=1; i<input; i++){
      for (j=1; j<input; j++){
        if (i==j || (i+j)==input){
          System.out.print(" ");
        }
        else{
          System.out.print("*");
        }
      }
      System.out.println();
    }
  }
}
          