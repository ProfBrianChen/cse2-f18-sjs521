/* Sebastian Soman
CSE 02 Convert class
9.18.18
This class takes the dimensions input by the user regarding the affected area of a hurricane and the rainfall within said area then calculates and prints the quantity of rain in cubic miles. 
*/
import java.util.Scanner;

public class Convert{
  
  public static void main(String args[]){
    
    Scanner scnr = new Scanner(System.in);
    System.out.print("Enter the affected area in acres: "); //number of acres of land affected by hurricane precipitation
    double areaInAcres = scnr.nextDouble();
    System.out.print("Enter the rainfall in the affected area: "); //number of inches of rain dropped on average in affected area
    final double GALLON_TO_CUBIC_MILE = 9.0817e-13; //constant variable representing the conversion rate of 1 gallon to 1 cubic mile
    double inchesOfRain = scnr.nextDouble();
    double cubicFeet = (43560 * areaInAcres) / 12.0; //43560 is the amount of feet in 1 acre
    double cubicInches = 1728 * inchesOfRain; //12^3 is a the amount of cubic inches in 1 cubic foot
    double gallons = (cubicFeet * cubicInches) / 231.0; //231 is the amount of cubic inches in 1 gallon
    double cubicMiles = gallons * GALLON_TO_CUBIC_MILE; //converts total gallons to cubic miles
   
    System.out.print(cubicMiles + " cubic miles");
    
    
    
    
    
    
    
  }
}