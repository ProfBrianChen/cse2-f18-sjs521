/* Sebastian Soman
CSE 02 Pyramid Class
9.18.18
This class takes the dimensions input by the user regarding a square-based pyramid then calculates and prints the volume.
*/
import java.util.Scanner;

public class Pyramid{
  
  public static void main(String args[]){
    
    Scanner scnr = new Scanner(System.in);
    System.out.print("The square side of the pyramid is (input length): ");
    double length = scnr.nextDouble(); //user input = length
    System.out.print("The height of the pyramid is (input height): ");
    double height = scnr.nextDouble(); //user input = height
    double volume = (length * length * height) / 3.0; //equation for volume of a square-based pyramid
    
    System.out.println("The volume inside the pyramid is: " + volume); //prints the calculated volume
  }
}