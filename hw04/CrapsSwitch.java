/* Sebastian Soman
   CSE 02 
   CrapsSwitch Class
   This program prints the slogan for 2 rolled die in a Craps game
   */

import java.util.Scanner;

public class CrapsSwitch{
  
  public static void main(String args[]){
    
    Scanner scnr = new Scanner(System.in);
    System.out.print("If you would like to randomly cast dice, enter 'random.' If you would like to state the two dice, enter 'choose.'");
    String input = scnr.nextLine();
    int dice1 = 0;
    int dice2 = 0; 
    String slang = "";
    if (input.equals("random")){
      dice1 = (int) ((Math.random() * 7) + 1); //random number for die1
      dice2 = (int) ((Math.random() * 7) + 1); //random number for die2
    }
    else if (input.equals("choose")){
      System.out.println("Enter an integer between 1 and 6 (inclusive) to be your first die");
      dice1 = scnr.nextInt(); //associates user input with die1
      System.out.println("Enter an integer between 1 and 6 (inclusive) to be your second die");
      dice2 = scnr.nextInt(); //associates user input with die2
    }
    
    //determines every possible slogan
    switch (dice1){
      case 1:
        switch(dice2){
          case 1:
            slang = "snake eyes";
            break;
          case 2:
            slang = "Ace deuce";
            break;
          case 3:
            slang = "easy four";
              break;
          case 4:
            slang = "Fever five";
            break;
          case 5:
            slang = "easy six";
            break;
          case 6:
            slang = "seven out";
            break;
        }
        break;
      case 2:
        switch(dice2){
          case 1: 
            slang = "Ace deuce";
            break;
          case 2:
            slang = "Hard four";
            break;
          case 3:
            slang = "fever five";
            break;
          case 4:
            slang = "Easy six";
            break;
          case 5:
            slang = "Seven out";
            break;
          case 6:
            slang = "easy eight";
            break;
        }
        break;
      case 3:
        switch(dice2){
          case 1: 
            slang = "Easy four";
            break;
          case 2:
            slang = "fever five";
            break;
          case 3:
            slang = "Hard six";
            break;
          case 4:
            slang = "Seven out";
            break;
          case 5:
            slang = "easy eight";
            break;
          case 6:
            slang = "Nine";
            break;
        }
        break;
      case 4:
        switch(dice2){
            case 1: 
            slang = "Fever five";
            break;
          case 2:
            slang = "Hard six";
            break;
          case 3:
            slang = "Seven out";
            break;
          case 4:
            slang = "easy eight";
            break;
          case 5:
            slang = "Nine";
            break;
          case 6:
            slang = "easy ten";
            break;
        }
        break;
      case 5:
        switch(dice2){
           case 1: 
            slang = "easy six";
            break;
          case 2:
            slang = "Seven out";
            break;
          case 3:
            slang = "easy eight";
            break;
          case 4:
            slang = "nine";
            break;
          case 5:
            slang = "Hard ten";
            break;
          case 6:
            slang = "Yo-leven";
            break;
        }
        break;
      case 6:
        switch(dice2){
          case 1: 
            slang = "Seven out";
            break;
          case 2:
            slang = "easy eight";
            break;
          case 3:
            slang = "nine";
            break;
          case 4:
            slang = "easy ten";
            break;
          case 5:
            slang = "Yo-leven";
            break;
          case 6:
            slang = "Boxcars";
            break;
        }
        
        
    }
    System.out.println(slang); //prints slogan
  }
}