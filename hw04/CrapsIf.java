/* Sebastian Soman
   CSE 02 
   CrapsSwitch Class
   This program prints the slogan for 2 rolled die in a Craps game
   */

import java.util.Scanner;

public class CrapsIf{
  
  public static void main(String args[]){
    
    Scanner scnr = new Scanner(System.in);
    System.out.print("If you would like to randomly cast dice, enter 'random.' If you would like to state the two dice, enter 'choose.'");
    String input = scnr.nextLine();
    int dice1 = 0;
    int dice2 = 0; 
    String slang = "";
    if (input.equals("random")){
      dice1 = (int) ((Math.random() * 7) + 1);
      dice2 = (int) ((Math.random() * 7) + 1);
    }
    else if (input.equals("choose")){
      System.out.println("Enter an integer between 1 and 6 (inclusive) to be your first die");
      dice1 = scnr.nextInt();
      System.out.println("Enter an integer between 1 and 6 (inclusive) to be your second die");
      dice2 = scnr.nextInt();
    }
    
    if (dice1 == 1)
      if (dice2 == 1){
       slang = "Snake Eyes";
      }
      else if (dice2 == 2){
        slang = "Ace Deuce";
        }
      else if (dice2 == 3){
        slang = "Easy Four";
      }
      else if (dice2 == 4){
        slang = "Easy Five";
      }
      else if (dice2 == 5){
        slang = "Easy Six";
      }
      else if (dice2 == 6){
        slang = "Seven Out";
      }
    if (dice1 == 2)
      if (dice2 == 1){
        slang = "ace deuce";
      }
      else if (dice2 == 2){
        slang = "Hard Four";
      }
      else if (dice2 == 3){
        slang = "Fever five";
      }
      else if (dice2 == 4){
        slang = "easy six";
      }
      else if (dice2 == 5){
        slang = "Seven out";
      }
      else if (dice2 == 6){
        slang = "easy eight";
      }
    if (dice1 == 3)
      if (dice2 == 1){
        slang = "easy four";
      }
      else if (dice2 == 2){
        slang = "fever five";
      }
      else if (dice2 == 3){
        slang = "Hard six";
      }
      else if (dice2 == 4){
        slang = "seven out";
      }
      else if (dice2 == 5){
        slang = "easy eight";
      }
      else if (dice2 == 6){
        slang = "nine";
      }
    if (dice1 == 4)
      if (dice2 == 1){
        slang = "fever five";
      }
      else if (dice2 == 2){
        slang = "easy six";
      }
      else if (dice2 == 3){
        slang = "seven out";
      }
      else if (dice2 == 4){
        slang = "hard eight";
      }
      else if (dice2 == 5){
        slang = "nine";
      }
      if (dice2 == 6){
        slang = "easy ten";
      }
    if (dice1 == 5)
      if (dice2 == 1){
        slang = "easy six";
     }
      else if (dice2 == 2){
        slang = "seven out";
      }
      else if (dice2 == 3){
        slang = "easy eight";
      }
      else if (dice2 == 4){
        slang = "nine";
      }
      else if (dice2 == 5){
        slang = "hard ten";
      }
      else if (dice2 == 6){
        slang = "Yo-leven";
      }
    if (dice1 == 6)
      if (dice2 == 1){
        slang = "seven out";
      }
      else if (dice2 == 2){
        slang = "easy eight";
      }
      else if (dice2 == 3){
        slang = "nine";
      }
      else if (dice2 == 4){
        slang = "easy ten";
      }
      else if (dice2 == 5){
        slang = "Yo-leven";
      }
      else if (dice2 == 6){
        slang = "Boxcars";
      }
    
        System.out.println(slang);
         
    }
  }