/*Sebastian Soman
 * CSE 002
 * RemoveElements
 * */

import java.util.Scanner;
import java.util.Random;

public class RemoveElements {
 public static void main(String[] arg) {
  Scanner scan = new Scanner(System.in);
  int num[] = new int[10];
  int newArray1[];
  int newArray2[];
  int index, target;
  String answer = "";
  do {
   System.out.print("Random input 10 ints [0-9]");
   num = randomInput();
   String out = "The original array is:";
   out += listArray(num);
   System.out.println(out);

   System.out.print("Enter the index ");
   index = scan.nextInt();
   newArray1 = delete(num, index);
   String out1 = "The output array is ";
   out1 += listArray(newArray1); // return a string of the form "{2, 3, -9}"
   System.out.println(out1);

   System.out.print("Enter the target value ");
   target = scan.nextInt();
   newArray2 = remove(num, target);
   String out2 = "The output array is ";
   out2 += listArray(newArray2); // return a string of the form "{2, 3, -9}"
   System.out.println(out2);

   System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
   answer = scan.next();
  } while (answer.equals("Y") || answer.equals("y"));
 }

 public static String listArray(int num[]) {
  String out = "{";
  for (int j = 0; j < num.length; j++) {
   if (j > 0) {
    out += ", ";
   }
   out += num[j];
  }
  out += "} ";
  return out;
 }
//fills array with random integers from and returns the array
 public static int[] randomInput() {
  int arrayRand[] = new int[10];
  Random rand = new Random();
  int i;
  for (i = 0; i < arrayRand.length; i++) {
   arrayRand[i] = rand.nextInt(9) + 0;
  }
  return arrayRand;
 }
 public static int[] delete(int num[], int index) {
  int i;
  int[] deleteArray = null;
  //checks if index is within array
  if(index < num.length - 1) {
  for (i = 0; i < num.length; i++) {
    if (i == index) {
      //makes new array one index value shorter 
    deleteArray = new int[num.length - 1];
    // transfers integers over exluding the index entered
    for (int j = 0; j < i; j++) {
     deleteArray[j] = num[j];
    }
    for (int k = i; k < num.length - 1; k++) {
     deleteArray[k] = num[k + 1];
    }
   }
  }
  }else{
    // error statement
   deleteArray = new int[num.length];
   System.out.println("Index is out of bounds");
   for (i = 0; i < num.length; i++) {
    //saves all integers over to new array
    deleteArray[i] = num[i];
   }
  }
  return deleteArray;
 }

// imports array and target number to search for
 public static int [] remove(int num[], int target){
  int i;
  int j = 0;
  int counter = 0;
  int[] newArray;
  //goes through imported array and counts how many times the target is repeated
  for(i = 0; i < num.length; i++) {
   if(num[i] == target) {
    ++counter;
   }
  }
  newArray = new int[num.length - counter];
  for (i = 0; i < num.length; i++) {
   //skips index if array value is equal to target
    if(num[i] == target) {
    continue;
   }
    //uses j to keep track of new array index while running through old array
    newArray[j] = num[i];
    ++j;
   
  }
 
  return newArray;
 }
}

