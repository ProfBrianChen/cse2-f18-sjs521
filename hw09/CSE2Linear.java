/*Sebastian Soman
 * CSE 002
 * Linear class
*/

import java.util.Scanner;
import java.util.Random;

public class CSE2Linear{
 public static void main(String[] args) {
  final int numElements = 15;
  int[] finalGrade = new int[numElements];
  int i;
  int userInput;
  Scanner scnr = new Scanner(System.in);
  System.out.println("Enter " + numElements + " ascending ints for final grades in CSE2: ");

  for (i = 0; i < finalGrade.length; ++i){
   //Checks if user's input is an integer
    if (!scnr.hasNextInt()){
    scnr.next();
    System.out.println("Error: Enter an integer");
    --i;
    continue;
   }
   finalGrade[i] = scnr.nextInt();
   //checks if grade is between 0 and 100
   if (finalGrade[i] > 100 || finalGrade[i] < 0){
    System.out.println("Error: Integer must be between 0-100");
    --i;
    continue;
   }
   
   if (i != 0) {
     //checks if integers being entered are in ascending order
    if (finalGrade[i] < finalGrade[i - 1]){
     System.out.println("Error: Integers must be in ascending order");
     --i;
     continue;
    }
   }
  }
  //prints the Array
  for (i = 0; i < finalGrade.length; i++) {
   System.out.print(finalGrade[i] + " ");
  }
  System.out.println();
  System.out.println("Enter a grade to search for: ");
  userInput = scnr.nextInt();
  binarySearch(finalGrade, userInput);
  int[] scrambledGrades = randomScramble(finalGrade);
  //prints scrambled grades
  System.out.println("Scrambled: ");
 for(i = 0; i < scrambledGrades.length; i++) {
  System.out.print(scrambledGrades[i] + " ");
 }
 System.out.println();
 //asks user to enter another integer to search for using linear search
 System.out.print("Enter a grade to search for: ");
 userInput = scnr.nextInt();
  linearSearch(scrambledGrades, userInput);
 }
//linear search method
 public static void linearSearch(int[] finalGrade, int userInput) {
  int i;
  //goes through array prints 
  for (i = 0; i < finalGrade.length; i++) {
   //prints if number is found
    if (finalGrade[i] == userInput) {
    System.out.println("Linear Search: " + userInput + " found after " + i + " iterations");
    break;
   }
   //prints a not found statement
   if (i == finalGrade.length - 1) {
    System.out.println("Linear Search: " + userInput + " not found after " + i + " iterations");
   }
  }
  System.out.println("");
 }
//Binary Search method
 public static void binarySearch(int[] finalGrade, int userInput) {
  boolean boolin = false;
  int low = 0;
  int high = finalGrade.length;
  int mid;
  int counter = 0;
 //binary search loop
  while(low < high){
    mid = (high + low)/2;
    ++counter;//counts how many times loop runs
    if(finalGrade[mid] == userInput){
      boolin = true;
      break;
    }
    if(finalGrade[mid] < userInput){
      low = mid + 1;
    }
    if(finalGrade[mid] > userInput){
      high = mid - 1;
    }
  }
  //if number is found prints how many interations were run
  if(boolin == true){
    System.out.println("Binary Search: " + userInput + " was found in the list with " + counter + " iterations");
  }else{
    //if number doesnt exist prins that users integer wasnt found
     System.out.println("Binary Search: Grade does not exist");
  }
 }
//scrable array method
 public static int[] randomScramble(int[] finalGrade) {
  int[] scrambledGrades = new int[finalGrade.length];
  int i;
  int j;
  Random rand = new Random();
  for (i = 0; i < finalGrade.length; i++) {
   scrambledGrades[i] = finalGrade[i];
  }
  //runs scramble loop 10000 times just make sure it is really scrambled
  for(j = 0; j < 10000; j++){
  for (i = 0; i < scrambledGrades.length; i++) {
   int randomIndex = rand.nextInt(scrambledGrades.length);
   int temp = scrambledGrades[i];
   scrambledGrades[i] = scrambledGrades[randomIndex];
   scrambledGrades[randomIndex] = temp;
  }
  }
  return scrambledGrades;//returns scrambledGrades array
 }

}


