/*Sebastian Soman
 * CSE 002
 * Tic-tac-toe
 * */
import java.util.*;
public class HW10{
  public static void main (String [] args){
    Scanner scan = new Scanner(System.in);
    int moves = 0; // counter to see whether or not there is a draw

    String [][] board = { 
        {"1", "2", "3"},
        {"4", "5", "6"},
        {"7", "8", "9"}
      };

    printBoard(board); // Prints board
    while (moves < 9){
      int player = 1;  // Set player = 1 so player 1 moves first


      while (player == 1){  
         System.out.println("Player 1, enter where you would like to put your O token");

         // Error message if user does not put in an int
         while (!scan.hasNextInt()){
           System.out.println("Error: Incorrect Type. Try Again.");
           scan.next();
         } 

         int place = scan.nextInt(); // sets place equal to the user's input
         String letter = "O";        // Player 1 uses "O"


         // Error message if number is out of range
         while (place < 1 || place > 9){
           System.out.println("Error: out of range. Try Again.");
           place = scan.nextInt();
         }

         board = player1(board, place); // sets board equal to the output of player1()

         printBoard(board); // Prints the board with new changes


         // Checks to see if gameWon() returns true (meaning the player won)
         if (gameWon(board, letter)){
           System.out.println("Player 1 you Won!");
           System.exit(0);
         }

         moves++; //incremements moves after player has moved
         if (moves == 9){ // if moves reaches 9, all spaces have been taken up, breaks out of loop
           break;
         }
         player++; // Increments player by one to be able to pass onto player 2
      }


      while (player == 2){
         System.out.println("Player 2, enter where you would like to put your X token");

         // Error message if user does not put in an int
         while (!scan.hasNextInt()){
           System.out.println("Error: Incorrect Type. Try Again.");
           scan.next();
         }

         int place = scan.nextInt(); // sets place equal to the user's input
         String letter = "X";        // Player 2 uses "X"


         // Error message if number is out of range 
         while (place < 1 || place > 9){
           System.out.println("Error: out of range. Try Again.");
           place = scan.nextInt();
         }

         board = player2(board, place); // sets board equal to the output of player1()

         printBoard(board); // Prints the board with new changes


         // Checks to see if gameWon() returns true (meaning the player won)
         if (gameWon(board, letter)){
           System.out.println("Player 2 you Won!");
           System.exit(0);
         }

         moves++; //incremements moves after player has moved
         player--; // Decrements player by one to be able to pass onto player 1
      }
    }


    // if moves reaches 9 without a win, prints draw message and exits
    System.out.println("It's a draw!");
    System.exit(0);  

  }


  /* Prints the board */
  public static void printBoard(String [][] board){
    for (int row = 0; row < board.length; row++){
      for (int column = 0; column < board[row].length; column++){
        System.out.print(board[row][column] + " ");
      }
      System.out.println();
    }
  }


  /* Method used for Player 1. Takes in the most updated board, and the place they choose,
   * to iterate through the array and put an O in the spot they want. */
  public static String [][] player1(String [][] board, int place){
    Scanner scan = new Scanner(System.in);

    // Converts int to String to be able to compare to String [][] board
    String stringPlace = Integer.toString(place);

    int counter = 0; // sets counter to 0 to enter loop
    while (counter == 0){ 
      for (int row = 0; row < board.length; row++){
        for (int column = 0; column < board[row].length; column++){

          if (board[row][column].equals(stringPlace)){ // When the place user wants is found
            board[row][column] = "O";                  // set place equal to "O",
            counter++;                                 // incremement counter by 1 
            break;                                     // and break out of loop

          }
        }
      }

      // if after iterating through the entire array the counter is still 0, then that means the
      // place was not found in the array because it had already been taken up by an "X" or "O"
      if (counter == 0){
        System.out.println("Error: This place is taken already. Try Again."); // prints error message
        stringPlace = scan.next();
      }
    }
    return board; //return updated String [][] board

  }


  /* Method used for Player 2. Takes in the most updated board, and the place they choose,
   * to iterate through the array and put an X in the spot they want. */
  public static String [][] player2(String [][] board, int place){
    Scanner scan = new Scanner(System.in);

    // Converts int to String to be able to compare to String [][] board
    String stringPlace = Integer.toString(place);

    int counter = 0; // sets counter to 0 to enter loop
    while (counter == 0){ 
      for (int row = 0; row < board.length; row++){
        for (int column = 0; column < board[row].length; column++){
          if (board[row][column].equals(stringPlace)){ // When the place user wants is found
            board[row][column] = "X";                  // set place equal to "X",
            counter++;                                 // incremement counter by 1 
            break;                                     // and break out of loop
          }
        }
      }

      // if after iterating through the entire array the counter is still 0, then that means the
      // place was not found in the array because it had already been taken up by an "X" or "O"
      if (counter == 0){
        System.out.println("Error: This place is taken already. Try Again."); // prints error message
        stringPlace = scan.next();
      }
    }
    return board; //return updated String [][] board

  }


  /* Method to check it the game has been won.
   * Takes in the most updated board and the letter that corresponds with the player.*/
  public static boolean gameWon(String [][] board, String letter){
    // i = row
    // j = column

    // Checks horizontal victory
    for (int i = 0; i < board.length; i++) {
      if ((board[i][0].equals(letter)) && (board[i][1].equals(letter)) && (board[i][2].equals(letter))) {
      return true;
      }
     }

     // Checks verticle victory
     for (int j = 0; j < board.length; j++) {
       if ((board[0][j].equals(letter)) && (board[1][j].equals(letter)) && (board[2][j].equals(letter))) {
       return true;
       }
     }

     // Checks diagonal victory(left to right)
     if ((board[0][0].equals(letter)) && (board[1][1].equals(letter)) && (board[2][2].equals(letter))) {
       return true;
     }

     // Checks diagonal victory(right to left)
     if ((board[0][2].equals(letter)) && (board[1][1].equals(letter)) && (board[2][0].equals(letter))) {
       return true;
     } 
     // If user has not won, return gameWon() as false
     else {
       return false;
     }
  }

}